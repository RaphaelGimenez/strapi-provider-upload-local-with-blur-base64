# strapi-provider-upload-local-with-blur-base64

## Description

A copy of the official strapi local provider with the difference that it stores a blur image as base64 in the field `provider_metadata.base64`.
This is done so that it integrates nicely with Next.js Image's blur attribute.


## Resources

- [LICENSE](LICENSE)

## Links

- [Strapi website](https://strapi.io/)
- [Strapi documentation](https://docs.strapi.io)
- [Strapi community on Discord](https://discord.strapi.io)
- [Strapi news on Twitter](https://twitter.com/strapijs)

## Installation

```bash
# using yarn
yarn add strapi-provider-upload-local-with-blur-base64

# using npm
npm install strapi-provider-upload-local-with-blur-base64 --save
```

## Configurations

This provider has only one parameter: `sizeLimit`.

### Provider Configuration

`./config/plugins.js`

```js
module.exports = ({ env }) => ({
  // ...
  upload: {
    config: {
      provider: 'strapi-provider-upload-local-with-blur-base64',
      providerOptions: {
        sizeLimit: 100000,
      },
    },
  },
  // ...
});
```

The `sizeLimit` parameter must be a number. Be aware that the unit is in bytes, and the default is 1000000. When setting this value high, you should make sure to also configure the body parser middleware `maxFileSize` so the file can be sent and processed. Read more [here](https://docs.strapi.io/developer-docs/latest/plugins/upload.html#configuration)

### Security Middleware Configuration

Special configuration of the Strapi Security Middleware is not required on this provider since the default configuration allows loading images and media from `"'self'"`.
